#include "DHT.h"
#include "FS.h"
#include "SPIFFS.h"
#include <TFT_eSPI.h>
#include <SPI.h>

#define DHTPIN 32     // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

#define FORMAT_SPIFFS_IF_FAILED true

DHT dht(DHTPIN, DHTTYPE);
TFT_eSPI tft = TFT_eSPI();
unsigned int lines;

int sigPin = 39;
int mulPin[4] = {27, 26, 25, 33};
int bPinDel = 35, bPinPrint = 0;
bool delConfirm = 0;

void setup() {
  for (int i = 0; i < 4; i++) pinMode(mulPin[i], OUTPUT);
  pinMode(bPinDel,INPUT_PULLUP);
  pinMode(bPinPrint,INPUT_PULLUP);

  dht.begin();
  
  tft.init();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_GREEN, TFT_BLACK);
  tft.setTextSize(4);

  if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)) {
    tft.setTextColor(TFT_RED, TFT_BLACK);
    tft.println("SPIFFS\nMount\nFailed");
    delay(1000);
    return;
  }
  
  lines = 0;
  tft.drawString("Reading",0,0,1);
  File file = SPIFFS.open("/data.raw");
  if(file) for(char c = file.read(); file.available(); c = file.read()) {
    if(c == '\n') lines++;
  }
  file.close();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_YELLOW, TFT_BLACK);
  tft.drawNumber(lines,0,0,1);
  
  if(digitalRead(bPinPrint) == LOW) {
    if(digitalRead(bPinDel) == LOW) {
      if(delConfirm == false) {
        tft.setTextColor(TFT_RED, TFT_BLACK);
        tft.drawString("Delete?",0,0,1);
        delConfirm = true;
      } else {
        SPIFFS.remove("/data.raw");
        lines = 0;
        tft.setTextColor(TFT_RED, TFT_BLACK);
        tft.drawString("Deleted!",0,0,1);
      }
    } else {
      tft.setTextColor(TFT_GREEN, TFT_BLACK);
      tft.drawString("Printing",0,0,1);
      Serial.begin(115200);
      File file = SPIFFS.open("/data.raw");
      while(file.available()) Serial.write(file.read());
      file.close();
    }
  } else {
    delConfirm = false;
  }

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  uint16_t sensor[9];
  for (int i = 0; i < 9; i++) {
    // Set multiplexer bits
    for (int j = 0; j < 4; j++) {
      int val = (i >> j) & 1 == 1 ? HIGH : LOW;
      digitalWrite(mulPin[j], val);
    }

    // Read out
    delay(1);
    sensor[i] = analogRead(sigPin);
  }

  // Write to file
  File file = SPIFFS.open("/data.raw", FILE_APPEND);
  file.printf("%.1f,%.1f,", t, h);
  for (int i = 0; i < 9; i++) {
    file.printf("%d", sensor[i]);
    if (i!=8) file.print(",");
  }
  file.printf("\n");
  file.close();
  lines++;
}
